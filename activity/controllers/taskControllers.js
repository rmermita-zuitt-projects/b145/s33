const Task = require('../models/taskSchema');

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
};


module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

module.exports.specificTask = () => {
	return Task.findOne({id: '_id'}).then(id => {
		return id
	});
};

module.exports.updateStatus = (taskId, reqBody) => {
	return Task.findByIdAndUpdate(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} 

		result.status = reqBody.status
			return result.save().then((updatedStatus, saveErr) => {
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedStatus
				}
			})
	})
}