const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post('/createTask', (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/tasks/:id", (req, res) => {
	taskControllers.specificTask().then(resultFromController => res.send(resultFromController));
});

router.put("/updateStatus/:id/complete", (req, res) => {
	taskControllers.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;