const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

// retrieving
router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

/*
	Mini-Activity
	1. Create a route for createTask controller, name your endpoint as /createTask
	2. Send your postman screenshot in hangouts
	ENDS IN 10 MINUTES- 7:55 PM
*/
// SOLUTION:
router.post('/createTask', (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// END OF SOLUTION

// Deleting
router.delete("/deleteTask/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Updating
router.put("/updateTask/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})




module.exports = router;