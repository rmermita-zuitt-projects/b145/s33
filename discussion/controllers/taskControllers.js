const Task = require('../models/taskSchema');

// Retrieving all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
};

// Creating a task
module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

// Delete Task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

// Update Task

module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} 

		result.name = reqBody.name
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
			})
	})
}