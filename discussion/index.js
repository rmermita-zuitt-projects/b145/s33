const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes')

const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://rem-ermita:pHa2retrac.@b145.s8xba.mongodb.net/session33?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true

	}
);

app.use('/', taskRoutes)

app.listen(port, () => console.log(`Server running at port ${port}`))